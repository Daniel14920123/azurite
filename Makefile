all:
	mcs -langversion:7 ./src/*.cs -out:azurite.exe
	clear && ledit mono azurite.exe

debug:
	mcs -debug -langversion:7 ./src/*.cs -out:azurite.exe
	clear && ledit mono --debug --debugger-agent=transport=dt_socket,server=y,address=127.0.0.1:55555 azurite.exe

debug_file:
	mcs -debug -langversion:7 ./src/*.cs -out:azurite.exe
	clear && ledit mono --debug --debugger-agent=transport=dt_socket,server=y,address=127.0.0.1:55555 azurite.exe -f ./tests/macro.azur -t ocaml -s ./tests/compiled

debug_cpp:
	mcs -debug -langversion:7 ./src/*.cs -out:azurite.exe
	clear && ledit mono --debug --debugger-agent=transport=dt_socket,server=y,address=127.0.0.1:55555 azurite.exe -f ./tests/macro.azur -t cpp -s ./tests/compiled

debug_repl_p:
	mcs -debug -langversion:7 ./src/*.cs -out:azurite.exe
	clear && ledit mono --debug --debugger-agent=transport=dt_socket,server=y,address=127.0.0.1:55555 azurite.exe -r ocaml -t ocaml

debug_repl:
	mcs -debug -langversion:7 ./src/*.cs -out:azurite.exe
	clear && ledit mono --debug --debugger-agent=transport=dt_socket,server=y,address=127.0.0.1:55555 azurite.exe

build:
	mcs -langversion:7 ./src/*.cs -out:azurite.exe

run:
	clear && ledit mono azurite.exe

run_file:
	ledit mono ./azurite.exe -f ./tests/macro.azur -t ocaml -s ./tests/compiled

run_cpp:
	ledit mono ./azurite.exe -f ./tests/macro.azur -t cpp -s ./tests/compiled

config:
	# [ -d ~/.azurite ] && rm -r ~/.azurite
	mkdir ~/.azurite

install:
	cp ./azurite.exe ~/.azurite/azurite.exe
	cp ./distribution/azurite ~/.azurite/azurite
	chmod +x ~/.azurite/azurite
	chmod +x ~/.azurite/azurite.exe
	cp ./AZPM/dist/azpm ~/.azurite/azpm
	cp ./AZPM/azpm.py ~/.azurite/azpm.py
	chmod +x ~/.azurite/azpm
	chmod +x ~/.azurite/azpm.py
	echo "PATH+=:~/.azurite/" >> ~/.bashrc
	azpm -install https://gitlab.com/Daniel14920123/azurite-standard-library

uninstall:
	rm -r ~/.azurite

install_all:
	make build
	make config
	make build
	make install
	rm azurite.exe
