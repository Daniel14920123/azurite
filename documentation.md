# Azurite Documentation


## Les Translates
> Les "translate" représentent la base du transpileur, ils permettent de décrire comment sont transpilées les S-expressions.

*Structure d'un translate*
```lisp
(translate (nom paramètres) ("type") (langage "import" "body"))
```

*Exemple:*
```lisp
(translate ("+" a b) ("num" "num" "num") (python "" "{a} + {b}"))
```
Ici est définis comment + est convertis en python

### Les paramètres

En Azurite, les translates s'appuient sur un système de pattern matching, ainsi les paramètres doivent être déclarés d'une manière spéciale.

Symbole | Exemple | Nom | Description
--- | --- | --- | ---
" " | "=" | Match fort | Lors de la recherche le jeton doit être exactement égal
' ' | 'name' | Match strict | Lors de la recherche le jeton doit être directement présent
[ ] | [function_name] | Callable | Lors de la recherche le jeton doit pouvoir être appelé
∅| x | Match faible | Lors de la recherche le jeton peut être n’importe quoi, il peut s’agir d’une S-Expression.
∅... | x.. | List | Lors de la recherche le jeton doit être une liste

### Le body
Le body ou le corps de l'expression est l'expression traduite dans le langage ciblé. 
Soit le translate suivant:
```lisp
(translate ("nom" 'light' x) (langage "" "si {light} faire {x}"))
```
Les paramètres sont situés entre accolade dans le corps, c'est à cet emplacement qu'ils seront remplacés.

### le typage
Les translates sont typés, pour le typage, on ne précise pas le type des Match fort exemple:

```lisp
(translate ("nom" 'num' "milieu" liste_str... ) ("num" "str..." "type_de_retour") (langage "" "do_something ..."))
```

### les types
Il existe plusieurs types de base en azurite:
- num : flottants
- str : chaînes
- bool : booléens
- unit : équivalent de void, en tant que type de retour, utilisé par exemple par print
- topunit : sert pour le type de retour d'une expression ne pouvant être placée dans une autre, par exemple une définition de fonction
- #nombre : désigne un type polymorphe d'id nombre. Tous les polymorphes au sein d'une même définition sont équivalents si leurs nombres sont équivalents
- type... : désigne une liste de type. Notez qu'une liste placée en fin de fonction ou d'expression peut-être interprétée comme un nombre variable d'arguments,suivant l'arité. Si elle est respectée, le jeton représente une liste. Sinon, tous les arguments dépassant seront considérés comme en faisant partie.

### valeur
Les expressions typées qui devront être placées dans les fonctions doivent représenter une valeur, par exemple on ne peut pas retourner une déclaration,
ainsi tous les corps de translate doivent pourvoir être typés. De plus, afin d'avoir une homoiconicité, il est préférable d'utiliser des listes.

### les imports
Il se peut qu'un translate ait besoin d'une bibliothèque externe ou d'une fonction d'aide pour fonctionner, on peut ainsi écrire ce codeen utilisant le champ import:
```lisp
(translate ("sqrt" x ) ("num" "num") (language "from Math import sqrt" "sqrt({x})"))
```

### Librairie standard
Une librairie standard a déjà été prototypée, trouvable dans ./stdlib/protos.azurprotos

### Les listes
> Ce paragraphe expliquera l'utilisation des listes dans le corps d'un translate

#### 1. Annoncer un paramètre comme étant une liste

Dans la partie paramètre de votre translate, il suffit de rajouter `...` à la suite du nom du paramètre pour que le dit paramètre soit considérer comme une liste. Il faudras ensuite typé ce parmètre comme étant une liste. cf partie typage.
```lisp
(translate (liste...) ("type..." "type_de_retour"))
```

#### 2. Écrire la conversion
 > En reprenant l'exemple d'au-dessus la conversion s'écrira à l'intérieur des balises suivante: `{liste }` respectant la syntaxe `{nom_du_paramètre }`

La conversion est une S-expressions sous la forme suivante:
```lisp
(liste separateur début fin)
```
exemple:
```lisp
(translate (liste...) ("type..." "type_de_retour") 
    (langage "" "{liste (liste , [ ])}"))
```
Séparateur seras la valeur avec laquelle tous les éléments seront séparés
début seras la valeur passé au tout début de la liste tandis que fin seras la valeur placé à la fin de la liste.
début et fin sont optionnels toutefois si l'un est précisé l'autre doit l'être aussi.
#### 3. Écriture avancée des listes.
La liste devant être sous la forme d'une S-expression elle seras donc parsé et les macro seront donc appliqué à cet arbre syntaxique, il est donc tout à fait possible d'utiliser des macro pour génerer le code d'une liste, il est ainsi possible d'avoir des S-expression plus compliqué en tant que separateur, début et fin.  
Il est aussi possible d'appliquer un translate à chaque élément d'une liste en suivant comme ceci: `((monTranslate liste) , [ ])` ici, lors de l'évaluation `liste` seras remplacé avec chaque élément de la liste.

#### Remarques:
Si vous souhaitez utiliser } comme token à l'intérieur de la balise vous devez forcément rajouté \\ devant.


#### Exemples :
```
(translate ("liste_simple" liste...) ("num..." "liste...") (ocaml "" "{liste (liste ; [ ])}"))
(liste_simple 1 2 3 4 5)
(// Attendus: [1;2;3;4;5])

(translate ("ajouter_un" nombre) ("num" "num") (ocaml "" "{nombre} + 1"))
(translate ("liste_compliquée" liste...) ("num..." "liste...") (ocaml "" "{liste ((ajouter_un liste) ; [ ])}"))
(liste_compliquée 1 2 3 4 5)
(// Attendus: [1+1;2+1;3+1;4+1;5+1])
```
