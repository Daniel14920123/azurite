﻿Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If install_extension.Checked Then
            Process.Start("C:\Users\Tbalta\OneDrive - EPITA\Bureau\cours\S2\Projet\azurite\distribution\windows\extension_install.bat")
            MessageBox.Show("Redémmarrez vscode pour que l'installation prenne effet.")
        End If
        If install_azurite.Checked Then
            Process.Start("install.bat")
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If uninstall_extension.Checked Then
            Process.Start("C:\Users\Tbalta\OneDrive - EPITA\Bureau\cours\S2\Projet\azurite\distribution\windows\extension_uninstall.bat")
            MessageBox.Show("Redémmarrez vscode pour que la désinstallation prenne effet.")
        End If
        If uninstall_azurite.Checked Then
            If MsgBox("Attention azurite ne seras pas retiré du path, veuillez l'enlevez manuellement." & Environment.NewLine & "Souhaitez vous continuer ?", MsgBoxStyle.YesNo, "...") = MsgBoxResult.Yes Then
                Process.Start("uninstall.bat")
            End If
        End If
    End Sub
End Class
