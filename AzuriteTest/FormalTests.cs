using System;
using Xunit;
namespace AzuriteTest
{
    public class FormalTests
    {
        private static string typeOfString(string input)
        {

            return Azurite.MyFormal.GetStupidType(new Azurite.Parser.SExpression(input))[0];
        }

        public class BasicFormalTests
        {
            public class TestAtome
            {
                [Fact]
                public void TestNum()
                {
                    Azurite.Lexer.init_builtins();
                    Assert.Equal("num", typeOfString("5"));
                }
                [Fact]
                public void TestNumNegat()
                {
                    Azurite.Lexer.init_builtins();
                    Assert.Equal("num", typeOfString("-5"));
                }
                [Fact]
                public void TestStr()
                {
                    Azurite.Lexer.init_builtins();
                    Assert.Equal("str", typeOfString("\"coucou\""));
                }
                [Fact]
                public void TestError()
                {
                    Azurite.Lexer.init_builtins();
                    Assert.Throws<Azurite.Azurite.Ezception>(() => typeOfString("coucou"));
                }
            }
            public class TestList
            {
                [Fact]
                public void TestNumList()
                {
                    Azurite.Lexer.init_builtins();
                    Assert.Equal("num...", typeOfString("(5)"));
                }
                [Fact]
                public void TestStrList()
                {
                    Azurite.Lexer.init_builtins();
                    Assert.Equal("str...", typeOfString("(\"coucou\")"));
                }
                [Fact]
                public void TestAnyList()
                {
                    Azurite.Lexer.init_builtins();
                    Assert.Equal("any", typeOfString("(5 \"str\")"));
                }
                [Fact]
                public void TestErrorList()
                {
                    Azurite.Lexer.init_builtins();
                    Assert.Throws<Azurite.Azurite.Ezception>(() => typeOfString("(coucou)"));
                }

            }
        }

        public class OperationFormalTests
        {
            private static void InitOpTests()
            {
                Azurite.Langconfig.load("C:/Users/tbalt/OneDrive - EPITA/Bureau/cours/S2/Projet/azurite/stdlib/langconfig.azur");
                Azurite.Azurite.Reset();
                Azurite.Lexer.init_builtins();
                Azurite.Azurite.Load(new string[]
                {
                    "(translate (\"+\" x1 x2) (\"num\" \"num\" \"num\") )",
                    "(translate (\"-\" x1 x2) (\"num\" \"num\" \"num\"))",
                    "(translate (\"*\" x1 x2) (\"num\" \"num\" \"num\"))",
                    "(translate (\"/\" x1 x2) (\"num\" \"num\" \"num\"))",
                }
                );

            }
            public class Addition
            {
                [Fact]
                public void AdditionSimple()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(+ 5 2)"));
                }
                [Fact]
                public void AdditionError()
                {
                    InitOpTests();
                    Assert.Throws<Azurite.Azurite.Ezception>(() => typeOfString("(+ 5 \"str\")"));
                }
                [Fact]
                public void AdditionAdvanced1()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(+ 5 (+ 2 3))"));
                }
                [Fact]
                public void AdditionAdvanced2()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(+ (+ 5 2) 2)"));
                }
                [Fact]
                public void AdditionAdvanced3()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(+ (+ 5 4) (+ 2 3))"));
                }
            }
            public class Soustraction
            {
                [Fact]
                public void SoustractionSimple()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(- 5 2)"));
                }
                [Fact]
                public void SoustractionError()
                {
                    InitOpTests();
                    Assert.Throws<Azurite.Azurite.Ezception>(() => typeOfString("(- 5 \"str\")"));
                }
                [Fact]
                public void SoustractionAdvanced1()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(- 5 (- 2 3))"));
                }
                [Fact]
                public void SoustractionAdvanced2()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(- (- 5 2) 2)"));
                }
                [Fact]
                public void SoustractionAdvanced3()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(- (- 5 4) (- 2 3))"));
                }
            }
            public class Multiplication
            {
                [Fact]
                public void MultiplicationSimple()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(* 5 2)"));
                }
                [Fact]
                public void MultiplicationError()
                {
                    InitOpTests();
                    Assert.Throws<Azurite.Azurite.Ezception>(() => typeOfString("(* 5 \"str\")"));
                }
                [Fact]
                public void MultiplicationAdvanced1()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(* 5 (* 2 3))"));
                }
                [Fact]
                public void MultiplicationAdvanced2()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(* (* 5 2) 2)"));
                }
                [Fact]
                public void MultiplicationAdvanced3()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(* (* 5 4) (* 2 3))"));
                }
            }
            public class Division
            {
                [Fact]
                public void DivisionSimple()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(/ 5 2)"));
                }
                [Fact]
                public void DivisionError()
                {
                    InitOpTests();
                    Assert.Throws<Azurite.Azurite.Ezception>(() => typeOfString("(/ 5 \"str\")"));
                }
                [Fact]
                public void DivisionAdvanced1()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(/ 5 (/ 2 3))"));
                }
                [Fact]
                public void DivisionAdvanced2()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(/ (/ 5 2) 2)"));
                }
                [Fact]
                public void DivisionAdvanced3()
                {
                    InitOpTests();
                    Assert.Equal("num", typeOfString("(/ (/ 5 4) (/ 2 3))"));
                }
            }


        }
    }
}
