#!/usr/bin/python3.8

import urllib
import requests
import os
import time
import re
import zipfile
import io
import argparse

import warnings

warnings.filterwarnings('ignore', message='Unverified HTTPS request')

INSTALL_DIR = os.path.expanduser("~/.azurite/")

def get_file(url):
    return requests.get(url, verify=False).text

def find_first_matching(to_match, data):
    first_part = to_match[0:to_match.find("|")]
    second_part = to_match[to_match.find("|")+1 : ]
    begin = data.find(first_part) + len(first_part)
    result = data[begin : data.find(second_part)]
    if result==data:
        return "error"
    return result

def find_last_matching(to_match, data):
    first_part = to_match[0:to_match.rfind("|")]
    second_part = to_match[to_match.rfind("|")+1 : ]
    begin = data.rfind(first_part) + len(first_part)
    result = data[begin : data.rfind(second_part)]
    if result==data:
        return "error"
    return result

class Package_info:
    def __init__(self, name: str, version, comment: str):
        self.name = name
        self.version = version
        self.comment = comment

    def get_version_as_str(self):
        (M, m, p) = self.version
        return str(int(M)) + "." + str(int(m)) + "." + str(int(p))

    def __ne__(self, other):
        pass


def read_config_file(content):
    comment = ""
    data = []
    i = 0
    for line in content:
        if line != "" and i <= 3:
            data.append(line)
        elif line != "":
            comment += line + "\n"
        i+=1
    data.append(comment)
    if len(data) != 5:
        raise SyntaxError("In provided package info file")
    return Package_info(data[0], (float(data[1]), float(data[2]), float(data[3])), data[4])

def get_info_from_gitlab(url: str):
    s = get_file(url.replace("/tree/", "/raw/").replace("/blob/", "/raw/") + "AzuritePackageInfo")
    return read_config_file(s.splitlines())

def get_info_from_install(name: str):
    return read_config_file(open(INSTALL_DIR + name + "/AzuritePackageInfo", 'r').read().splitlines())

def get_dependencies(pkg :Package_info):
    b = False
    l = pkg.comment.splitlines()
    c = 0
    r = []
    for i in l:
        if i == "Dependencies:":
            b = True
            continue
        if b:
            t = i.strip()
            if(t != ""):
                r.append(t)
    return r


def extract_project_name(url):
    pname = re.search(r'gitlab\.com/[^/]*/([^/]+)', url).group(1)
    return pname

def download_project(url, is_master, directory, name):
    pname = extract_project_name(url)
    if is_master:
        zip_name = pname + '-master.zip'
        download_url = f'{url}-/archive/master/{zip_name}'
    else:
        zip_name = pname + '-releasegj-2.0.0.zip'
        download_url = f'{url}-/archive/releasegj/2.0.0/{zip_name}'

    try:
        resp = requests.get(download_url, verify=False)
        z = zipfile.ZipFile(io.BytesIO(resp.content))
        #os.rename(directory + "/" + name, )
        #for i in z.namelist():
        #    z.extract(i , directory +"/"+ name)
        #print(pname)
        #z.extract(pname + "-master", directory)
        z.extractall(directory)
        os.rename(directory + pname + "-master", directory + name)
    except:
        raise Exception(f'Project {pname} download failed! Aborting.')

def install_from_gitlab(url: str):
    config = get_info_from_gitlab(url + "-/blob/master/")
    print("Package informations got for " + config.name + " in " + url + ".")
    if os.path.exists(INSTALL_DIR + config.name + "/AzuritePackageInfo"):
        temp = get_info_from_install(config.name)
        if temp.get_version_as_str() >= config.get_version_as_str():
            print("The package currently installed is up-to-date." + temp.name + ": (" + temp.get_version_as_str() + ">=" + config.get_version_as_str() +")")
            return
    for i in get_dependencies(config):
        i = i.strip()
        if i[-1] != "/":
            i += "/"
        print("Installing " + extract_project_name(i) + ", dependency of " + extract_project_name(url) + ".")
        install_from_gitlab(i)
    download_project(url, True, INSTALL_DIR, config.name)
    print(f"Installation of {config.name} finished.")

def get_names_from_install_config():
    pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Azurite Package Manager.")
    #parser.add_argument("Mode", metavar="mode", type=str, help="Install, remove or info.")
    parser.add_argument("-embed", "--embed", action="store_true", default=False, help="Embeds the package in current project directory.")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-install", "--install", action="store_true", help="Install a package from gitlab.")
    group.add_argument("-remove", "--remove", action="store_true", help="Remove an installed package.")
    group.add_argument("-info", "--info", action="store_true", help="Get info of an installed package.")
    parser.add_argument("Name", metavar="name", type=str, help="Url of the gitlab project in install mode, else package name")

    args = parser.parse_args()
    if args.embed:
        INSTALL_DIR = "./"
    if args.install:
        url = args.Name
        #url = input("Enter url to download :\t").strip()
        if url[-1] != "/":
            url += "/"
        install_from_gitlab(url)
    elif args.remove:
        pkg = args.Name
        __import__("shutil").rmtree(INSTALL_DIR + pkg)
        print(f"Package {pkg} removed successfully.")
    elif args.info:
        pkg = args.Name
        try:
            inf = get_info_from_install(pkg)
            print(f"+ Package {inf.name} :\n+ Version -> {inf.get_version_as_str()}\n+ Description -> {inf.comment}")
        except:
            print(f"Unable to find package {pkg} in current install.")
